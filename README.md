# SVG-2-PNG

Converts SVG to PNG.

## Configuration

Edit the parameters in [config.json](./config.json):

### Required Parameters

* `input:` Input directory containing the SVG files
* `output:` Output directory for PNG files

### Width and Height

Set `width` and `height` to `null` to keep the original size.

* `width:` Width of the PNG image
* `height:` Height of the PNG image

### Color and Opacity

Color and Opacity work for FontAwesome icons only. Set `color` to `null` to keep the original colors.

* `color:` Stroke and Fill color (first color)
* `opacity:` Opacity (first color)
* `color2:` Stroke and Fill color (second color, duotone icons only)
* `opacity2:` Opacity (second color, duotone icons only)

## Troubleshooting

Fix PhantomJS issue: `export OPENSSL_CONF=/etc/ssl/`