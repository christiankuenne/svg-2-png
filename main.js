const config = require('./config.json');
const converter = require('svg-to-png');
const fs = require('fs');
const path = require('path');

let options = null;

function main() {
    setOptions();
    processFiles();
}

function setOptions() {
    if (config.width && config.height) {
        options = {
            defaultWidth: config.width,
            defaultHeight: config.height
        }
    }
}

function processFiles() {
    // Create output dir
    if (!fs.existsSync(config.output)) {
        fs.mkdirSync(config.output);
    }
    // Read input dir
    fs.readdir(config.input, (err, filenames) => {
        if (err) {
            console.error(err);
            return;
        }
        filenames.forEach((filename) => {
            if (filename.toLowerCase().endsWith(".svg")) {
                convert(filename);
            }            
        });
    });
}

function convert(filename) {
    if (config.color) {
        // Color change: need to read file content
        fs.readFile(getPath(config.input, filename), 'utf-8', (err, content) => {
            if (err) {
                console.error(err);
                return;
            }
            // Add color attributes
            content = manipulateSvg(content);
            // Save temp file
            var tempFile = getPath(config.output, filename)
            fs.writeFile(tempFile, content, (err) => {
                if (err) {
                    console.error(err);
                    return;
                }
                // Convert temp file to png
                toPng(tempFile, (err) => {
                    if (err) {
                        console.log(err);
                        return;
                    }
                    // Delete temp file
                    fs.unlink(tempFile, (err) => {
                        if (err) {
                            console.error(err);
                        }                        
                    })
                });
            });
        });
    } else {
        // No color change: simple conversion
        toPng(getPath(config.input, filename), (err) => {
            if (err) {
                console.error(err);
            }        
        });
    }
}

function toPng(pathAndFilename, callback) {
    console.log(pathAndFilename);
    converter.convert(pathAndFilename, config.output, options)
        .then(() => {
            callback();
        })
        .catch((err) => {
            callback(err);
        })
}

function getPath(directory, filename) {
    if (directory.endsWith(path.sep) || directory.endsWith("/") || directory.endsWith("\\")) {
        directory = directory.slice(0, -1);
    }
    return `${directory}${path.sep}${filename}`
}

function manipulateSvg(content) {
    var startIndex = content.indexOf("<style>");
    var endIndex = content.indexOf("</style>") + 8;
    if (startIndex < 0 || endIndex <= startIndex) {
        // No <style> attribute. Set first color in path
        return content.split('<path ').join(`<path stroke="${config.color}" fill="${config.color}" `);
    } else {
        // Replace <style attribute>
        return content.substring(0, startIndex) + getStyle() + content.substring(endIndex);
    }    
}

function getStyle() {
    return `<style>.fa-primary{stroke:${config.color}; fill:${config.color}; opacity:${config.opacity}} .fa-secondary{stroke:${config.color2}; fill:${config.color2}; opacity:${config.opacity2}}</style>`;
}

main();
